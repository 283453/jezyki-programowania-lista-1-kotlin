fun <T> subsets(inputSet: Set<T>): List<Set<T>> { /*Rozpoczyna definicję generycznej funkcji
subsets, która przyjmuje zbiór elementów inputSet i zwraca listę wszystkich podzbiorów tego zbioru.*/
    require(inputSet.isNotEmpty()) { "Zbior nie moze byc pusty" } /* Sprawdza, czy zbiór wejściowy
    inputSet nie jest pusty. Jeśli jest, generuje wyjątek z odpowiednim komunikatem.*/


    val powerSet = mutableListOf<Set<T>>() /* Inicjalizuje pustą listę powerSet, która będzie
    przechowywać wszystkie podzbiory zbioru wejściowego. */
    val inputList = inputSet.toList() /* Konwertuje zbiór inputSet na listę, aby ułatwić indeksowanie.*/
    val n = inputList.size /* Określa liczbę elementów w liście, czyli rozmiar zbioru wejściowego */

/* Rozpoczyna pętlę for, która będzie przechodzić od 0 do 2^n - 1, gdzie n to liczba elementów w
 zbiorze wejściowym. Ta pętla generuje wszystkie możliwe kombinacje podzbiorów.
 */
    for (i in 0 until (1 shl n)) {
        val subset = mutableSetOf<T>() /* Tworzy pusty zmienny zbiór subset, który będzie reprezentować kolejny podzbiór.*/
        for (j in 0 until n) { /* Rozpoczyna pętlę wewnętrzną, która przechodzi przez indeksy elementów zbioru wejściowego.*/
            if (i and (1 shl j) != 0) { /* Sprawdza, czy j ma wartość 1. Jeśli tak, oznacza to,
            że j-ty element jest zawarty w aktualnie analizowanym podzbiorze.*/
                subset.add(inputList[j]) /* Dodaje j-ty element do aktualnie tworzonego podzbioru */
            }
        }
        powerSet.add(subset) /* Dodaje zakończony podzbiór do listy powerSet */
    }
    return powerSet /* Zwraca listę zawierającą wszystkie podzbiory zbioru wejściowego.*/
}


fun main() { /*  Rozpoczyna funkcję main(), która jest punktem wejścia programu.*/
    val x = setOf('a', 'b', 'c', 'd') /* Tworzy przykładowy zbiór x */
    val allSubsets = subsets(x) /* Wywołuje funkcję subsets dla zbioru x i przechowuje wynik w zmiennej allSubsets.*/
    allSubsets.forEach { println(it) } /*  Dla każdego podzbioru w allSubsets, drukuje ten podzbiór na ekranie.*/
}

@autor Karolina Majka
pomoc przy pętli - sztuczna inteligencja + wyjaśnienie od kolegi. Jeszcze czytałam o niej kilka
        na wykładach i kotlinowych dokumentach.