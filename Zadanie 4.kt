/*  Rozpoczyna definicję funkcji fibonacciRecursive, która przyjmuje argument n typu Int
i zwraca listę liczb całkowitych
 */
fun fibonacciRecursive(n: Int): List<Int> {
    require(n > 0) { "n musi byc dodatnia liczba calkowita" } /*Sprawdza warunek, czy n jest
    większe od zera. Jeśli nie, wywołuje funkcję require z komunikatem o błędzie, który
    zostanie wyświetlony w przypadku niepowodzenia warunku.*/


    val fibonacciList = mutableListOf<Int>() /*Inicjalizuje zmienną fibonacciList jako pustą
     listę liczb całkowitych, która może być modyfikowana */
    fibonacciRecursiveHelper(n, 0, 1, fibonacciList)
    return fibonacciList
}
/* Wywołuje rekurencyjnie funkcję fibonacciRecursiveHelper, zmniejszając wartość n o 1 i
aktualizując wartości a i b tak, aby obliczyć kolejną wartość ciągu.
 */

fun fibonacciRecursiveHelper(n: Int, a: Int, b: Int, list: MutableList<Int>) {
    if (n == 0) return
    list.add(a)
    fibonacciRecursiveHelper(n - 1, b, a + b, list)
}

/* Rozpoczyna funkcję main(), która jest punktem wejścia programu.*/
fun main() {
    val n = 10 /* Inicjalizuje zmienną n jako 10, określającą ilość elementów w ciągu Fibonacciego.*/
    try {  /*Rozpoczyna blok try-catch, aby obsłużyć ewentualny wyjątek IllegalArgumentException.*/
        val fibonacciSequence = fibonacciRecursive(n) /* Wywołuje funkcję fibonacciRecursive
        dla wartości n i przechowuje wynik w zmiennej fibonacciSequence.*/
        println("Ciag Fibonacciego dla pierwszych $n elementow: $fibonacciSequence") /*Wyświetla
        ciąg Fibonacciego dla pierwszych n elementów.*/
    } catch (e: IllegalArgumentException) {
        println(e.message) /* Wyświetla komunikat błędu w przypadku wystąpienia IllegalArgumentException.*/
    }
}
@autor Karolina Majka
Korzystałam z pomocy stron internetowych na temat ciągu
z youtuba gdzie opowiadają o podstawach kotlina
z dokumentacji kotlinowskiej
i z pomocy sztucznej inteligencji.