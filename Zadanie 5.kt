
/* Na początku definiujemy funkcje o nazwie collatz. Funkcja ta przyjmuje pojedynczy argument
* typu Int o nazwie c0 i zwraca listę liczb całkowitych.
 */
fun collatz(c0: Int): List<Int> {
    /* Następnie tworzymy  zmienną "sequence", która jest mutowalną listą liczb całkowitych,
    początkowo zawierającą tylko wartość c0
     */
    val sequence = mutableListOf(c0)
/*  Ustawiam zmienną "current" której wartością jest c0. Będzie ona aktualną wartością ciągu. */
    var current = c0

/* Dalej robię pętle  która będzie kontynuowana do momentu wyjścia z niej przez instrukcję break.*/
    while (true) {
        /* Aktualizuje wartość current według reguł sekwencji Collatza:
        jeśli current jest parzyste, dzieli je przez 2,
        w przeciwnym razie mnoży je przez 3 i dodaje 1.
         */
        current = if (current % 2 == 0) {
                        current / 2
        } else {
            3 * current + 1
        }

/* Sprawdza, czy aktualna wartość current jest już w sekwencji.
Jeśli tak, to oznacza to, że sekwencja osiągnęła cykl i pętla jest przerywana.
 */
        if (sequence.contains(current)) {
            break
        }

/* Dodaje aktualną wartość current do sekwencji.*/
        sequence.add(current)
    }

/* Zwraca sekwencję po zakończeniu pętli*/
    return sequence
}

/* funkcje collatzTests() oraz main() wywołują funkcję collatz() i przetwarzają wyniki
w celu znalezienia maksymalnej wartości w sekwencji Collatza oraz odpowiadającego jej c0,
który zaczyna tę sekwencję. Następnie te informacje są wyświetlane na ekranie za pomocą println().
 */
fun collatzTests() {
    var maxValue = 0
    var maxLength = 0
    var correspondingC0 = 0


    for (c0 in 1..1000) {
        val sequence = collatz(c0)
        if (sequence.size > maxLength) {
            maxLength = sequence.size
            maxValue = sequence.maxOrNull() ?: 0
            correspondingC0 = c0
        }
    }


    println("Maksymalna wartość elementu ciągu: $maxValue")
    println("Maksymalna długość ciągu przed wpadnięciem w cykl: $maxLength")
    println("Wartość c0 dla maksymalnej długości ciągu: $correspondingC0")
}


fun main() {
    collatzTests()
}

@autor Karolina Majka
        Korzystałam z pomocy stron internetowych na temat działania ciągu collatza,
        z youtuba gdzie opowiadają o podstawach kotlina
        z dokumentacji kotlinowskiej
        i z pomocy sztucznej inteligencji.