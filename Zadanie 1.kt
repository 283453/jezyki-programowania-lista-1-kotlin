import kotlin.math.sqrt

/* Funkcja,która dla zadanych boków a,b i c oblicza i zwraca pole trójkąta S
* wykorzystując wzór Herona.
*/
fun heron(a: Double, b: Double, c: Double): Double {
    /* Sprawdzenie warunków początkowych */
    if (a <= 0 || b <= 0 || c <= 0) {
        throw IllegalArgumentException("Długości boków muszą być dodatnie.")
    }
/* Sprawdzenie warunku trójkąta*/
if (a + b <= c || a + c <= b || b + c <= a) {
    throw IllegalArgumentException("Trójkąt o podanych bokach nie istnieje.")
}
/* Obliczenie pola trójkąta za pomocą wzoru Herona*/
val s = (a + b + c) / 2
val area = sqrt(s * (s - a) * (s - b) * (s - c))
return area
}
fun main() {
try {
    /* Przykładowe wywołanie funkcji*/
    val a = 5.0
    val b = 4.0
    val c = 3.0
    val area = heron(a, b, c)
    println("Pole trójkąta o bokach $a, $b, $c wynosi $area")
} catch (e: IllegalArgumentException) {
    println(e.message)
}
}

autor Karolina Majka
        pomoc Ania Sawicka, dużo było wyjaśnione na zajęciach
        skróty - kotlin docs