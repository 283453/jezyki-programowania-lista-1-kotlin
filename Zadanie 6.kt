/* Zaczynamy od zadeklarowania funkcji "komplement", która przyjmuje parametr "sekwencja"
typu String i zwraca String. */
fun komplement(sekwencja: String): String {
    /* Następnie tworzymy mapę komplementarności.
    Przyporządkowuje ona każdej zasadzie azotowej zawartej w DNA jej komplementarną zasadę.
    Czyli A=T i C=(potrójne wiązanie)G
     */
    val komplementarnosc = mapOf('A' to 'T', 'T' to 'A', 'C' to 'G', 'G' to 'C')
    /* W kolejnym kroku prosimy o odwrócenie sekwencji parametru "sekwencja".
    Następnie program ma przyporządkować każdą zasadę azotową w DNA z zasadą jej komplementarną
    zgodnie z mapą "komplementarnosc".
    Na końcu ma zwrócić połączoną sekwencję jako pojedyńczy ciągu znaków.*/
    return sekwencja.reversed().map { komplementarnosc[it] }.joinToString("")
}
/*Deklaracja funkcji transkrybuj, która przyjmuje parametr sekwencja typu String i zwraca String.*/
fun transkrybuj(sekwencja: String): String {
    /* Tworzenie mapy transkrypcja, która przyporządkowuje każdej zasadzie DNA jej odpowiednik w RNA.*/
    val transkrypcja = mapOf('A' to 'U', 'T' to 'A', 'C' to 'G', 'G' to 'C')
    /* Zmapowanie każdej zasady DNA na jej odpowiednik w RNA zgodnie z mapą transkrypcja,
    * a następnie połączenie wyników w pojedynczy ciąg znaków.
     */
    return sekwencja.map { transkrypcja[it] }.joinToString("")
}

fun main() {
    /* Deklaracja zmiennej sekwencjaKodujacaDNA przechowującej sekwencję kodującą DNA.*/
    val sekwencjaKodujacaDNA = "ATCGATCGATCG"
    /* Wywołanie funkcji komplement z sekwencją kodującą DNA jako argument i przypisanie jej
    wyniku do zmiennej sekwencjaMatrycowaDNA.
     */
    val sekwencjaMatrycowaDNA = komplement(sekwencjaKodujacaDNA)
    /* Wywołanie funkcji transkrybuj z sekwencją matrycową DNA jako argument i przypisanie jej
    wyniku do zmiennej sekwencjaRNA */
    val sekwencjaRNA = transkrybuj(sekwencjaMatrycowaDNA)
/* Wydrukowanie sekwencji kodującej DNA, sekwencji matrycowej DNA i sekwencji RNA */
    println("Sekwencja kodująca DNA: $sekwencjaKodujacaDNA")
    println("Sekwencja matrycowa DNA: $sekwencjaMatrycowaDNA")
    println("Sekwencja RNA: $sekwencjaRNA")
}


@autor Karolina Majka
        korzystałam z pomocy sztucznej inteligencji
        z filmików na youtubie z podstaw kotlina 
