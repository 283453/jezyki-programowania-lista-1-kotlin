/*  Definiuje funkcję generyczną czescWspolna, która przyjmuje dwa argumenty typu listy x i y i
zwraca listę zawierającą część wspólną elementów z listy x i y.
 */
fun <T> czescWspolna(x: List<T>, y: List<T>): List<T> {
    /*  Sprawdza, czy obie listy x i y nie zawierają wartości null. Jeśli zawierają,
    zostanie zgłoszony wyjątek z odpowiednim komunikatem.
     */
    require(x.all { it != null } && y.all { it != null }) {
        "Multizbiory nie mogą zawierać wartości null" }
/* Tworzy mapę, w której kluczem jest każdy element z listy x, a wartością jest liczba pojawienia
się tego elementu w liście x
 */
    val xCounts = x.groupingBy { it }.eachCount()
    /* to samo co wyżej tylko dla listy y */
    val yCounts = y.groupingBy { it }.eachCount()
/* Tworzy pustą listę commonPart, która będzie przechowywać część wspólną elementów z list x i y.*/
    val commonPart = mutableListOf<T>()
    /* Rozpoczyna pętlę, która przechodzi przez wszystkie pary klucz-wartość w mapie xCounts.*/
    for ((key, value) in xCounts) {
        /* Sprawdza, czy klucz z mapy xCounts istnieje również w mapie yCounts.*/
        if (yCounts.containsKey(key)) {
            /*  Oblicza minimalną liczbę pojawienia się danego klucza w listach x i y. */
            val minCount = minOf(value, yCounts[key]!!)
            /* Dodaje klucz do listy commonPart tyle razy, ile wynosi minimalna liczba jego
             pojawienia się w obu listach.
             */
            repeat(minCount) {
                commonPart.add(key)
            }
        }
    }
    /* Zwraca listę commonPart, która zawiera część wspólną elementów z list x i y.*/
    return commonPart
}
/* Rozpoczyna funkcję main(), która jest punktem wejścia programu*/
fun main() {
    val x = listOf(1, 1, 2, 3, 4) /* Inicjalizuje listę x zawierającą elementy.*/
    val y = listOf(1, 2, 2, 3, 3, 5) /* Inicjalizuje listę y zawierającą elementy.*/
    val commonPart = czescWspolna(x, y) /*  Wywołuje funkcję czescWspolna z listami x i y,
                                            a wynik przypisuje do zmiennej commonPart. */
    println("Część wspólna: $commonPart") /*  Wyświetla na konsoli część wspólną elementów z list x i y.*/
}


@autor Karolina Majka
        pomoc - Kotlin Docs
        wykłady
        pomoc przy require - AI